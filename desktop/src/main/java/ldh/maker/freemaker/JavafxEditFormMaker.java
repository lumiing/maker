package ldh.maker.freemaker;

import ldh.database.Table;

/**
 * Created by ldh on 2017/4/16.
 */
public class JavafxEditFormMaker extends BeanMaker<JavafxEditFormMaker> {

    protected Table table;
    protected String projectPackage;

    public JavafxEditFormMaker table(Table table) {
        this.table = table;
        return this;
    }

    public JavafxEditFormMaker projectPackage(String projectPackage) {
        this.projectPackage = projectPackage;
        return this;
    }

    @Override
    public JavafxEditFormMaker make() {
        data();
        out(ftl, data);

        return this;
    }

    @Override
    public void data() {
        fileName = table.getJavaName() + "EditForm.fxml";
        data.put("table", table);
        data.put("projectPackage", projectPackage);
        data.put("controllerPackage", pack);
    }
}
