package ldh.maker;

import ldh.maker.component.JavafxClientContentUiFactory;
import ldh.maker.util.UiUtil;

public class DeskMain extends ServerMain {

    @Override
    protected void preHandle() {
        UiUtil.setContentUiFactory(new JavafxClientContentUiFactory());
    }

    protected String getTitle() {
        return "javafx ui--代码自动生成工具";
    }

    public static void main(String[] args) throws Exception {
        startDb(args);
        launch(args);
    }
}

