package ldh.maker.freemaker;

import ldh.maker.vo.DBConnectionData;

/**
 * Created by ldh on 2017/4/8.
 */
public class ApplicationPropertiesMaker extends FreeMarkerMaker<ApplicationPropertiesMaker> {

    protected String projectRootPackage;
    protected String dbName;
    protected DBConnectionData dBConnectionData;

    public ApplicationPropertiesMaker projectRootPackage(String projectRootPackage) {
        this.projectRootPackage = projectRootPackage;
        return this;
    }

    public ApplicationPropertiesMaker dbName(String dbName) {
        this.dbName = dbName;
        return this;
    }

    public ApplicationPropertiesMaker dBConnectionData(DBConnectionData dBConnectionData) {
        this.dBConnectionData = dBConnectionData;
        return this;
    }


    @Override
    public ApplicationPropertiesMaker make() {
        data();
        if (ftl == null) {
            this.out("applicationProperties.ftl", data);
        } else {
            this.out(ftl, data);
        }
        return this;
    }

    @Override
    public void data() {
        fileName = "application.properties";
        data.put("projectRootPackage", projectRootPackage);
        data.put("dbConnectionData", dBConnectionData);
        data.put("dbName", dbName);
    }
}
