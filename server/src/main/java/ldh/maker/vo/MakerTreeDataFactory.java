package ldh.maker.vo;


import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by ldh on 2017/2/25.
 */
public class MakerTreeDataFactory {

    private static MakerTreeDataFactory instance = null;

    private ConcurrentMap<String, TreeNode> projectTreeDataMap = new ConcurrentHashMap<>();

    public static MakerTreeDataFactory getInstance() {
        if (instance == null) {
            synchronized (MakerTreeDataFactory.class) {
                if (instance == null) {
                    instance = new MakerTreeDataFactory();
                }
            }
        }
        return instance;
    }

    public ConcurrentMap<String, TreeNode> getProjectTreeDataMap() {
        return projectTreeDataMap;
    }
}
