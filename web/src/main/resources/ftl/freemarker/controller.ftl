package ${package};

<#if imports?exists>
	<#list imports as import>
import ${import};
	</#list>
</#if>

@Controller
@RequestMapping("${util.firstLower(bean)}")
public class ${className} <#if implements?exists>extends <#list implements as implement>${implement}<#if implement_has_next>,</#if></#list></#if> ${r'{'}

	@Resource(name = "${util.firstLower(service.simpleName)}")
	private ${service.simpleName} ${util.firstLower(service.simpleName)};
	
	<#if util.hasDate(table)>
	/**
	 * 字符串转化成时间
	 */
	@InitBinder  
	protected void initBinder(HttpServletRequest request,  
	                              ServletRequestDataBinder binder) throws Exception {  
	    //对于需要转换为Date类型的属性，使用DateEditor进行处理  
	    binder.registerCustomEditor(Date.class, new DateEditor());  
	} 
	</#if>

	<#list table.indexies as index>
	<#if index.primaryKey>
    @RequestMapping(method = RequestMethod.POST, value = "/save")
    public String save(@ModelAttribute ${beanWhereMaker.simpleName} ${util.firstLower(beanWhereMaker.simpleName)}) throws Exception {
    	Objects.requireNonNull(${util.firstLower(beanWhereMaker.simpleName)});
		${bean} ${util.firstLower(bean)} = (${bean}) ${util.firstLower(beanWhereMaker.simpleName)};
    	if (${util.firstLower(beanWhereMaker.simpleName)}.get${util.firstUpper(table.primaryKey.keyName)}() == null) {
			${util.firstLower(service.simpleName)}.insert(${util.firstLower(beanWhereMaker.simpleName)});
    	} else {
			${util.firstLower(service.simpleName)}.updateBy${util.uniqueName(index)}(${util.firstLower(beanWhereMaker.simpleName)});
    	}
    	return "redirect:/${lowerBean}/${lowerBean}List";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/save/json")
    @ResponseBody
    public String saveJson(@ModelAttribute ${beanWhereMaker.simpleName} ${util.firstLower(beanWhereMaker.simpleName)}) throws Exception {
    	Objects.requireNonNull(${util.firstLower(beanWhereMaker.simpleName)});
    	${bean} ${util.firstLower(bean)} = (${bean}) ${util.firstLower(beanWhereMaker.simpleName)};
    	if (${util.firstLower(beanWhereMaker.simpleName)}.get${util.firstUpper(table.primaryKey.keyName)}() == null) {
			${util.firstLower(service.simpleName)}.insert(${util.firstLower(beanWhereMaker.simpleName)});
		} else {
			${util.firstLower(service.simpleName)}.updateBy${util.uniqueName(index)}(${util.firstLower(beanWhereMaker.simpleName)});
		}
        return JsonViewFactory.create()
				.toJson();
    }
	</#if>
	</#list>

	
	@RequestMapping(method = RequestMethod.GET, value = "/toEdit/{id}")
    public String toEdit(@PathVariable ${util.firstUpper(table.primaryKey.column.javaType)} id, Model model) throws Exception {
		${bean} ${util.firstLower(bean)} = ${util.firstLower(service.simpleName)}.getBy${util.firstUpper(table.primaryKey.column.property)}(id);
    	model.addAttribute("${util.firstLower(bean)}", ${util.firstLower(bean)});
		<#list table.columnList as column>
			<#if util.isEnum(column)>
		model.addAttribute("${column.property}Values", ${table.javaName}${util.firstUpper(column.property)}Enum.values());
			</#if>
		</#list>
		model.addAttribute("active", "${util.lowers(bean)}");
        return "${lowerBean}/${lowerBean}Edit";
    }
    
	@RequestMapping(method = RequestMethod.GET, value = "/view/{id}")
    public String view(@PathVariable ${util.firstUpper(table.primaryKey.column.javaType)} id, Model model) throws Exception {
		Objects.requireNonNull(id);
    	${bean} ${util.firstLower(bean)} = ${util.firstLower(service.simpleName)}.getBy${util.firstUpper(table.primaryKey.column.property)}(id);
    	model.addAttribute("${util.firstLower(bean)}", ${util.firstLower(bean)});
		model.addAttribute("active", "${util.lowers(bean)}");
        return "${lowerBean}/${lowerBean}View";
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/view/json/{id}")
    @ResponseBody
    public String viewJson(@PathVariable ${util.firstUpper(table.primaryKey.column.javaType)} id, Model model) throws Exception {
		Objects.requireNonNull(id);
    	${bean} ${util.firstLower(bean)} = ${util.firstLower(service.simpleName)}.getBy${util.firstUpper(table.primaryKey.column.property)}(id);
    	return JsonViewFactory.create()
				.setDateFormat("yyyy-MM-dd hh:mm:ss")
				.put("data", ${util.firstLower(bean)})
			<#list table.columnList as column>
				<#if util.isEnum(column)>
				.registerTypeAdapter(${column.javaType}.class, new ValuedEnumObjectSerializer())
				</#if>
			</#list>
				.toJson();
    }
	
	@RequestMapping(method = RequestMethod.GET, value = "/list")
	public String list(@ModelAttribute ${beanWhereMaker.simpleName} ${util.firstLower(beanWhereMaker.simpleName)}, Model model) {
		${util.firstLower(beanWhereMaker.simpleName)}.setOrder("${table.primaryKey.column.name} desc");
		<#if table.foreignKeys?? && table.foreignKeys?size gt 0 && util.isCreate(table, "findByJoin")>
		PageResult<${bean}> ${util.firstLower(bean)}s = ${util.firstLower(service.simpleName)}.findJoinBy${bean}Where(${util.firstLower(beanWhereMaker.simpleName)});
		<#else>
		PageResult<${bean}> ${util.firstLower(bean)}s = ${util.firstLower(service.simpleName)}.findBy${bean}Where(${util.firstLower(beanWhereMaker.simpleName)});
		</#if>
		model.addAttribute("${util.firstLower(bean)}s", ${util.firstLower(bean)}s);
		model.addAttribute("active", "${util.lowers(bean)}");
		return "${lowerBean}/${lowerBean}List";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/list/json")
	@ResponseBody
	public String listJson(@ModelAttribute ${beanWhereMaker.simpleName} ${util.firstLower(beanWhereMaker.simpleName)}, Model model) {
		${util.firstLower(beanWhereMaker.simpleName)}.setOrder("${table.primaryKey.column.name} desc");
		<#if table.foreignKeys?? && table.foreignKeys?size gt 0 && util.isCreate(table, "findByJoin")>
		PageResult<${bean}> ${util.firstLower(bean)}s = ${util.firstLower(service.simpleName)}.findJoinBy${bean}Where(${util.firstLower(beanWhereMaker.simpleName)});
		<#else>
		PageResult<${bean}> ${util.firstLower(bean)}s = ${util.firstLower(service.simpleName)}.findBy${bean}Where(${util.firstLower(beanWhereMaker.simpleName)});
		</#if>
		return JsonViewFactory.create()
				.setDateFormat("yyyy-MM-dd hh:mm:ss")
				.put("data", ${util.firstLower(bean)}s)
			<#list table.columnList as column>
			<#if util.isEnum(column)>
			    .registerTypeAdapter(${column.javaType}.class, new ValuedEnumObjectSerializer())
			</#if>
			</#list>
				.toJson();
	}
	
	<#--@RequestMapping(method = RequestMethod.GET, value = "/delete/json/{id}")-->
	<#--@ResponseBody-->
	<#--public String deleteJson(@PathVariable ${util.firstUpper(table.primaryKey.column.javaType)} id) {-->
		<#--${bean} ${lowerBean} = ${util.firstLower(service.simpleName)}.getById(id);-->
		<#--Assert.notNull(${lowerBean});-->
		<#--${util.firstLower(service.simpleName)}.delete(id);-->
		<#---->
		<#--return JsonViewFactory.create()-->
				<#--.toJson();-->
	<#--}-->

	<#-----------------------------------delete----------------------------------------------------------------------------------->
	<#list table.indexies as index>
		<#if util.isUnique(index, table)>
        <#if index.primaryKey>
	@RequestMapping(method = RequestMethod.GET, value = "/deleteBy${util.uniqueName(index)}/json${util.uniqueParamControllerPath(table, index)}")
	@ResponseBody
	public String deleteJsonBy${util.uniqueName(index)}(${util.uniqueParamController(table, index)}) {
		${bean} ${lowerBean} = ${util.firstLower(service.simpleName)}.getBy${util.uniqueName(index)}(${util.uniqueValue(index)});
        Objects.requireNonNull(${lowerBean});
		${util.firstLower(service.simpleName)}.deleteBy${util.uniqueName(index)}(${util.uniqueValue(index)});
        return JsonViewFactory.create()
				.toJson();
        }

		</#if>
        </#if>
	</#list>
	
	<#--
	@RequestMapping(method = RequestMethod.GET, value = "/enable/{id}")
	public String enable(@PathVariable ${util.firstUpper(table.primaryKey.column.javaType)} id) {
		${bean} ${lowerBean} = ${util.firstLower(service.simpleName)}.getById(id);
		if (${lowerBean}.getStatus() == Status.disable) {
			${lowerBean}.setStatus(Status.enable);
			${lowerBean}.setUpdateTime(new Date());
			${util.firstLower(service.simpleName)}.save(${lowerBean});
		}
		
		return "forward:/${lowerBean}/list";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/disable/{id}")
	public String disable(@PathVariable ${util.firstUpper(table.primaryKey.column.javaType)} id) {
		${bean} ${lowerBean} = ${util.firstLower(service.simpleName)}.getById(id);
		if (${lowerBean}.getStatus() == Status.enable) {
			${lowerBean}.setStatus(Status.disable);
			${lowerBean}.setUpdateTime(new Date());
			${util.firstLower(service.simpleName)}.save(${lowerBean});
		}
		
		return "forward:/${lowerBean}/list";
	}
	-->
${r'}'}