<${r'#'}include "/macro/publicMacro.ftl">
<${r'#'}import "/macro/pagination.ftl" as Pagination>
<${r'#'}import "/template/macro/FormItem.ftl" as Form>

<${r'@'}header title="${util.comment(table)}简介">
    <link href="/resource/frame/datetimepicker/jquery.datetimepicker.min.css" rel="stylesheet">
</${r'@'}header>

<${r'@'}body>
<h2>${util.comment(table)}编辑</h2>
<form class="form-horizontal">
    <input type="hidden" name="${table.primaryKey.column.property}" value="${r'${'}${util.firstLower(table.javaName)}.${table.primaryKey.column.property}${r'}'}"/>
    <#list table.columnList as column>
        <#if column.foreign>
    <${r'@'}Form.FormItem name="${column.property}" label="${util.comment(column)}">
        <input type="text" class="form-control" id="${column.property}" aria-describedby="${util.comment(column)}" placeholder="${util.comment(column)}" value="${r'${'}${util.firstLower(table.javaName)}.${column.property}.${column.foreignKey.foreignTable.columnList[0].property}${r'}'}">
    </${r'@'}Form.FormItem>
        <#elseif util.isPrimaryKey(table, column)>
        <#elseif util.isDate(column)>
    <${r'@'}Form.FormItem name="${column.property}" label="${util.comment(column)}">
        <div class='input-group date' id='${column.property}1'>
            <input type="text" class="form-control" id="${column.property}" aria-describedby="${util.comment(column)}" placeholder="${util.comment(column)}" value="${r'${('}${util.firstLower(table.javaName)}.${column.property}?string('yyyy-MM-dd hh:mm:ss')${r')!}'}">
            <span class="input-group-addon">
                     <span class="glyphicon glyphicon-calendar"></span>
                </span>
        </div>
    </${r'@'}Form.FormItem>
        <#elseif util.isNumber(column)>
    <${r'@'}Form.FormItem name="${column.property}" label="${util.comment(column)}">
        <input type="text" class="form-control" id="${column.property}" aria-describedby="${util.comment(column)}" placeholder="${util.comment(column)}" value="${r'${'}${util.firstLower(table.javaName)}.${column.property}${r'}'}">
    </${r'@'}Form.FormItem>
        <#elseif util.isEnum(column)>
    <${r'@'}Form.FormItem name="${column.property}" label="${util.comment(column)}">
        <select class="form-control" id="${column.property}">
            <${r'#'}list ${column.property}Values as ${column.property}>
                <option value="${r'${'}${column.property}.value${r'}'}" <${r'#'}if ${column.property} == ${util.firstLower(table.javaName)}.${column.property}${r'>'}selected</${r'#'}if>>${r'${'}${column.property}.desc${r'}'}</option>
            </${r'#'}list>
        </select>
    </${r'@'}Form.FormItem>
        <#else>
    <${r'@'}Form.FormItem name="${column.property}" label="${util.comment(column)}">
        <input type="text" class="form-control" id="${column.property}" aria-describedby="${util.comment(column)}" placeholder="${util.comment(column)}" value="${r'${'}${util.firstLower(table.javaName)}.${column.property}${r'}'}">
    </${r'@'}Form.FormItem>
        </#if>
    </#list>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>
</${r'@'}body>

<${r'@'}footer>
    <script src="/resource/frame/datetimepicker/jquery.datetimepicker.full.min.js"></script>
    <script type="text/javascript">
        $(function () {
            <#list table.columnList as column>
            <#if util.isDate(column)>
            $('#${column.property}').datetimepicker({
                format:'Y-m-d H:i:s',
                lang:'zh'});
            </#if>
            </#list>
        });
    </script>
</${r'@'}footer>