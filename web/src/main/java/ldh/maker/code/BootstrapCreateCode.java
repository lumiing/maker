package ldh.maker.code;

import javafx.scene.control.TreeItem;
import ldh.database.Table;
import ldh.maker.database.TableInfo;
import ldh.maker.freemaker.*;
import ldh.maker.util.CopyDirUtil;
import ldh.maker.util.DbInfoFactory;
import ldh.maker.util.FileUtil;
import ldh.maker.util.FreeMakerUtil;
import ldh.maker.vo.SettingData;
import ldh.maker.vo.TreeNode;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;

/**
 * Created by ldh on 2017/4/6.
 */
public class BootstrapCreateCode extends WebCreateCode {

    public BootstrapCreateCode(SettingData data, TreeItem<TreeNode> treeItem, String dbName, Table table) {
        super(data, treeItem, dbName, table);
//        jspFtls.put("/bootstrap/jspList.ftl", "");
        add("/bootstrap/head.ftl", "head.jsp", "jsp", "common");
        add("/bootstrap/left.ftl", "left.jsp", "jsp", "common");
        add("/bootstrap/mainTag.ftl", "main.tag", "tags");
        add("/bootstrap/jspMain.ftl", "main.jsp", "jsp");
        addData("controllerFtl", "/bootstrap/controller.ftl");
//        jsFtls.add("/bootstrap/jsList.ftl");
    }

    @Override
    protected void createOther(){
        super.createOther();

        String db = treeItem.getValue().getData().toString();
        TableInfo tableInfo = DbInfoFactory.getInstance().get(treeItem.getValue().getParent().getId() + "_" + db);
        String t = FreeMakerUtil.firstLower(table.getJavaName());
        String fileName = t.toLowerCase() + ".ftl";
        new JspMaker()
                .tableInfo(tableInfo)
                .table(table)
                .outPath(createJspPath("jsp", FreeMakerUtil.javaName(table.getName())))
                .ftl("/bootstrap/jspList.ftl")
                .fileName(t + "List.jsp")
                .make();

        new JspMaker()
                .tableInfo(tableInfo)
                .table(table)
                .outPath(createJspPath("jsp", FreeMakerUtil.javaName(table.getName())))
                .ftl("/bootstrap/jspView.ftl")
                .fileName(t + "View.jsp")
                .make();

        List<String> dirs = new ArrayList<>(Arrays.asList("code", root, this.getProjectName(), "src", "main", "webapp", "resource"));
        List<String> dirst = new ArrayList<>(Arrays.asList("code", root, getProjectName(), "src", "main", "webapp", "WEB-INF"));
        try {
            copyResources("common/js", dirs, "common", "js");
            copyResources("common/bootstrap", dirs, "common");
            copyResources("frame", dirs, "frame");
            copyResources("tags", dirst, "tags");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void buildPomXmlMaker(String path) {
        String projectRootPackage = getProjectRootPackage(data.getPojoPackageProperty());
        String resourcePath = createPomPath();
        new PomXmlMaker()
                .projectRootPackage(projectRootPackage)
                .project(this.getProjectName())
                .ftl("/bootstrap/pom.ftl")
                .outPath(resourcePath)
                .make();
    }

    public String getProjectName() {
        return "bootstrap-web";
    }
}
